package runners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty"
                , "html:target/reports/cucumber-pretty"
                , "json:target/reports/androidJson/cucumber.json"
                , "html:target/reports/default-cucumber-reports"
                , "junit:target/reports/junit/cucumber-results.xml"
        }
        , features = "features"
        , glue = "android/step_definitions"
        , dryRun = false
        , tags = "@regression"
)

public class AndroidRunner {
}
