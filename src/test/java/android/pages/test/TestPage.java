package android.pages.test;

import helpers.config.Driver;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestPage {

    public TestPage() {
        PageFactory.initElements(Driver.getInstance(), this);
    }

    //================================================================================
    // LOCATORS
    //================================================================================

    @FindBy(xpath = "//img[@alt='Google']")
    public static WebElement googleLogo;

    @FindBy(xpath = "//input[@name='q']")
    public static WebElement searchInput;

    //================================================================================
    // ACTIONS
    //================================================================================

    public void openPage(String url) {
        Driver.getInstance().get(url);
    }

    public void searchQuery(String query) {
        googleLogo.click();
        searchInput.sendKeys(query);
        searchInput.sendKeys(Keys.ENTER);
    }

    public void closePage() {
        Driver.closeDriver();
    }
}