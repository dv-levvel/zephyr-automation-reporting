package android.step_definitions;

import helpers.Utilities;
import helpers.ZephyrAPIManager;
import helpers.config.Driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.net.URISyntaxException;

public class Hooks {
    private static String testID;
    private static String response;

    @Before
    public void setUp(Scenario scenario) throws URISyntaxException {
        testID = Utilities.getString(scenario.getName(), "([aA-zZ]*-\\d*)");
        response = ZephyrAPIManager.createExecution(testID);
//        AppiumServer.start();
//        Driver.getInstance().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(Scenario scenario) throws URISyntaxException {
        int status = 1;//pass
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) Driver.getInstance()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
            status = 2;//fail
        }
        ZephyrAPIManager.updateExecution(testID, status, response);
        Driver.closeDriver();
//        AppiumServer.stop();
    }
}
