package android.step_definitions.test;

import android.pages.test.TestPage;

import io.cucumber.java.en.*;

public class Test {
    TestPage TestPage = new TestPage();

    @Given("The user logs into the application")
    public void the_user_logs_the_application() {
        System.out.println("Given");
    }

    @When("The user opens the web page {string}")
    public void the_user_opens_the_google_app(String url) {
        System.out.println("When");
        TestPage.openPage(url);
    }

    @When("The user types {string} and hit enter")
    public void the_user_types_and_hit(String query) {
        System.out.println("When");
        TestPage.searchQuery(query);
    }

    @Then("The console will print a message")
    public void the_console_will_print_a_message() {
        System.out.println("Success");
        TestPage.closePage();
    }

}