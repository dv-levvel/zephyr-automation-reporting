package android.step_definitions.convergence;

import android.pages.convergence.ZephyrTestPage;

import helpers.Utilities;
import helpers.ZephyrAPIManager;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.*;

import java.net.URISyntaxException;
import java.util.Properties;

public class ZephyrTest {
    ZephyrTestPage ZephyrTestPage = new ZephyrTestPage();
    Properties zephyrProperties = Utilities.extractProperties("src/test/resources/jira.properties");

    @Given("The user having the correct endpoint")
    public void user_have_endpoint() {
        System.out.println("Starting test");
    }

    @When("The user executes the endpoint")
    public void the_user_executes_the_endpoint() throws URISyntaxException {
        String url = "cycles/search?" +
                "versionId=" + zephyrProperties.getProperty("cVersionID") +
                "&projectId=" + zephyrProperties.getProperty("cProjectID");
//        ZephyrAPIManager.getCyclesList(url);
        System.out.println("Print Results");
    }

    @Then("Print the result")
    public void print_the_result() {
        System.out.println("Print Results");
    }
}
