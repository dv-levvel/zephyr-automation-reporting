package helpers;

import java.util.Hashtable;

public class DataManager {

    private DataManager(){ }

    private static DataManager _instance;

    private static Hashtable<String,Object> _data = new Hashtable<String,Object>();

    public static DataManager getInstance()
    {
        if(_instance == null)
            _instance = new DataManager();
        return _instance;
    }

    public void add(String key, Object value){
        _data.put(key, value);
    }

    public Object get(String key){
        return _data.get(key);
    }
}
