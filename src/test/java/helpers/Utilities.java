package helpers;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    private static Properties properties;

    private static void getConfigProperties(String path) {
        try {
            FileInputStream stream = new FileInputStream(path);
            properties = new Properties();
            properties.load(stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Properties extractProperties(String path) {
        getConfigProperties(path);
        return properties;
    }

    public static String getString(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
            return matcher.group(1);
        return "No match!";
    }

    public static String[] getCurrentDateTime(String datePattern, String timePattern) {
        Date newDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        SimpleDateFormat timeFormat = new SimpleDateFormat(timePattern);

        return new String[]{dateFormat.format(newDate), timeFormat.format(newDate)};
    }

    public static int getNumberFromString(String str) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find())
            return Integer.parseInt(matcher.group(0));
        else
            return -1;
    }
}
