package helpers;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Properties;

public class JiraAPIManager extends ZephyrAPIManager {

    private static final Properties jiraProperties = Utilities.extractProperties("src/test/resources/jira.properties");
    private static final String username = jiraProperties.getProperty("USERNAME");
    private static final String password = jiraProperties.getProperty("PASSWORD");

    private static final HttpClient restClient = new DefaultHttpClient();

    private static String generateBasicAuthToken() throws UnsupportedEncodingException {
        return Base64.getEncoder()
                .encodeToString((username + ":" + password)
                        .getBytes("UTF-8"));
    }

    public static Integer getIssueId(String issueKey) {
        try {
            HttpGet request = new HttpGet(
                    jiraProperties.getProperty("BASEURL") +
                            jiraProperties.getProperty("GET_ISSUE") + issueKey);
            request.addHeader("Authorization", "Basic " + generateBasicAuthToken());

            String response = generateResult(restClient.execute(request));
            JSONObject jiraIssueJSON = new JSONObject(response);
            return Integer.parseInt(jiraIssueJSON.getString("id"));
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

//    public static void getIssueIdJira(String url) {
//        try {
//            URL url2 = new URL(url);
//            String encodedAuth = Base64.getEncoder().encodeToString((username + ":" + password).getBytes("UTF-8"));
//
//            HttpURLConnection connection = (HttpURLConnection) url2.openConnection();
//            connection.setRequestMethod("GET");
//            connection.setDoOutput(true);
//            connection.setRequestProperty("Authorization", "Basic " + encodedAuth);
//            InputStream content = (InputStream) connection.getInputStream();
//            BufferedReader in =
//                    new BufferedReader(new InputStreamReader(content));
//            String line;
//            while ((line = in.readLine()) != null) {
//                JSONObject jiraIssueJSON = new JSONObject(line);
//                System.out.println(jiraIssueJSON.getString("id"));
//                System.out.println(line);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
