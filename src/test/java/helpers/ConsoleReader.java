package helpers;

import javax.annotation.Nonnull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConsoleReader extends Thread {
    final InputStream inputStream;
    final String type;
    final StringBuilder stringBuilder;

    public ConsoleReader(@Nonnull final InputStream inputStream, @Nonnull String type) {
        this.inputStream = inputStream;
        this.type = type;
        this.stringBuilder = new StringBuilder();
    }

    public void run() {
        try {
            final InputStreamReader isr = new InputStreamReader(inputStream);
            final BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                this.stringBuilder.append(line).append("\n");
            }
        } catch (final IOException ioe) {
            System.err.println(ioe.getMessage());
            throw new RuntimeException(ioe);
        }
    }

    @Override
    public String toString() {
        return this.stringBuilder.toString();
    }

    public static String getStdOut(String query) {
        try {
            final Process p = Runtime.getRuntime().exec(String.format("cmd /c %s", query));
            final ConsoleReader stderr = new ConsoleReader(p.getErrorStream(), "STDERR");
            final ConsoleReader stdout = new ConsoleReader(p.getInputStream(), "STDOUT");
            stderr.start();
            stdout.start();

            final int exitValue = p.waitFor();
            if (exitValue == 0) return stdout.toString();
            else return stderr.toString();
        } catch (final IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
