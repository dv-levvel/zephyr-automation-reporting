package helpers;

import com.thed.zephyr.cloud.rest.ZFJCloudRestClient;
import com.thed.zephyr.cloud.rest.client.JwtGenerator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class ZephyrAPIManager {
    private static final Properties zephyrProperties = Utilities.extractProperties("src/test/resources/zephyr.properties");

    private static final String jiraBaseUrl = zephyrProperties.getProperty("JIRA_BASEURL");
    private static final String zephyrBaseUrl = zephyrProperties.getProperty("ZEPHYR_BASEURL");
    private static final String accessKey = zephyrProperties.getProperty("ACCESS_KEY");

    private static final HttpClient restClient = new DefaultHttpClient();
    private static ZFJCloudRestClient client;

    private static void setZFJCLoudRestClient() {
        if (client == null)
            client = ZFJCloudRestClient.restBuilder(
                    zephyrBaseUrl,
                    accessKey,
                    zephyrProperties.getProperty("SECRET_KEY"),
                    zephyrProperties.getProperty("ACCOUNT_ID")
            ).build();
    }

    public static String generateToken(URI uri, String requestMethod, Integer expirationInSec) {
        setZFJCLoudRestClient();
        JwtGenerator jwtGenerator = client.getJwtGenerator();

        return jwtGenerator.generateJWT(requestMethod, uri, expirationInSec);
    }

    public static void getGeneralConfiguration() throws URISyntaxException {
        get(new URI(zephyrBaseUrl + "/public/rest/api/1.0/config/generalinformation"));
    }

    public static void getCyclesList(String url) throws URISyntaxException {
        get(new URI(zephyrBaseUrl + "/public/rest/api/1.0/" + url));
    }

    public static String createExecution(String TestID) throws URISyntaxException {
        JSONObject status = new JSONObject();
        status.put("id", 1);

        JSONObject executionDataObject = new JSONObject();
        executionDataObject.put("status", status);
        executionDataObject.put("issueId", JiraAPIManager.getIssueId(TestID));
        executionDataObject.put("projectId", Integer.parseInt(zephyrProperties.getProperty("cProjectID")));
        executionDataObject.put("versionId", Integer.parseInt(zephyrProperties.getProperty("cVersionID")));
        executionDataObject.put("cycleId", zephyrProperties.getProperty("cRegressionCycleID"));
        executionDataObject.put("assigneeType", "QA");
        executionDataObject.put("assignee", zephyrProperties.getProperty("ACCOUNT_ID"));

        StringEntity executionDataJSON = null;
        try {
            executionDataJSON = new StringEntity(executionDataObject.toString());
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        return post(new URI(zephyrBaseUrl + "/public/rest/api/1.0/execution"), executionDataJSON);
    }

    public static void updateExecution(String testID, Integer execStatus, String response) throws URISyntaxException {
        JSONObject executionJSON = new JSONObject(response);
        JSONObject executionDetails = executionJSON.getJSONObject("execution");
        String executionID = executionDetails.getString("id");

        JSONObject status = new JSONObject();
        status.put("id", execStatus);

        JSONObject executionDataObject = new JSONObject();
        executionDataObject.put("status", status);
        executionDataObject.put("issueId", JiraAPIManager.getIssueId(testID));
        executionDataObject.put("projectId", Integer.parseInt(zephyrProperties.getProperty("cProjectID")));
        executionDataObject.put("versionId", Integer.parseInt(zephyrProperties.getProperty("cVersionID")));
        executionDataObject.put("cycleId", zephyrProperties.getProperty("cRegressionCycleID"));
        executionDataObject.put("assigneeType", "QA");
        executionDataObject.put("assignee", zephyrProperties.getProperty("ACCOUNT_ID"));

        StringEntity executionDataJSON = null;
        try {
            executionDataJSON = new StringEntity(executionDataObject.toString());
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        put(new URI(zephyrBaseUrl + "/public/rest/api/1.0/execution/" + executionID), executionDataJSON);
    }

    public static void getExecutionList(String url) throws URISyntaxException {
        JSONObject executionListObject = new JSONObject();
        executionListObject.put("maxRecords", 20);
        executionListObject.put("offset", 0);

        StringEntity executionListJSON = null;
        try {
            executionListJSON = new StringEntity(executionListObject.toString());
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        post(new URI(zephyrBaseUrl + "/public/rest/api/1.0/" + url), executionListJSON);
    }

    private static void get(URI uri) {
        String JWT = generateToken(uri, "GET", 360);

        HttpGet request = new HttpGet(uri);
        request.addHeader("Authorization", JWT);
        request.addHeader("zapiAccessKey", accessKey);

        try {
            generateResult(restClient.execute(request));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String post(URI uri, StringEntity body) {
        String JWT = generateToken(uri, "POST", 360);

        HttpPost request = new HttpPost(uri);
        request.addHeader("Content-Type", "application/json");
        request.addHeader("Authorization", JWT);
        request.addHeader("zapiAccessKey", accessKey);
        request.setEntity(body);

        try {
            return generateResult(restClient.execute(request));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String put(URI uri, StringEntity body) {
        String JWT = generateToken(uri, "PUT", 360);

        HttpPut request = new HttpPut(uri);
        request.addHeader("Content-Type", "application/json");
        request.addHeader("Authorization", JWT);
        request.addHeader("zapiAccessKey", accessKey);
        request.setEntity(body);

        try {
            return generateResult(restClient.execute(request));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String generateResult(HttpResponse response) {
        int statusCode = response.getStatusLine().getStatusCode();
        String string;

        if (statusCode >= 200 && statusCode < 300) {
            HttpEntity entity = response.getEntity();
            try {
                string = EntityUtils.toString(entity);
                System.out.println(string);
                return string;
            } catch (ParseException | IOException e) {
                e.printStackTrace();
                return "" + e;
            }
        } else {
            try {
                throw new ClientProtocolException("Unexpected response status: " + statusCode);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return "" + e;
            }
        }
    }
}