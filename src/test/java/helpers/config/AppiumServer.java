package helpers.config;

import helpers.ConsoleReader;
import helpers.Utilities;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Properties;

public class AppiumServer {

    private static final Properties _properties = Utilities.extractProperties("src/test/resources/appium.properties");
    private static AppiumDriverLocalService _service;

    public static void main(String[] args) throws InterruptedException {
        AppiumServer.start();
        Thread.sleep(2000);
        AppiumServer.stop();
    }

    public static void start() {
        try {
            if (_service == null) {
                _service = getInstance(isDefaultPortAvailable());
                _service.start();
                System.out.println("Server started!!");
            }
        } catch (Exception e) {
            System.out.println("Cause: " + e.getCause());
            System.out.println("Message: " + e.getMessage());
        }
    }

    public static void stop() {
        try {
            int PID = Utilities.getNumberFromString(getServicePID(_service.getUrl().getPort()));
            String killProcessQuery = "taskkill /F /PID " + PID;
            ConsoleReader.getStdOut(killProcessQuery);
            System.out.println("Server stopped!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isDefaultPortAvailable() {
        try {
            ServerSocket serverSocket = new ServerSocket();
            serverSocket.setReuseAddress(false);
            serverSocket.bind(new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 4723), 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static String getServicePID(int port) {
        String netstatQuery = "netstat -aon | findstr 0.0.0.0:" + port;
        String netstatOut = ConsoleReader.getStdOut(netstatQuery);
        String pidQuery = "FOR /F \"tokens=5 delims= \" %G IN (\"" + netstatOut + "\") DO %G";
        return ConsoleReader.getStdOut(pidQuery);
    }

    private static AppiumDriverLocalService getInstance(Boolean defaultPort) {
        String[] dateTime = Utilities.getCurrentDateTime("yyyyMMdd", "HHmmss");
        String directoryPath = _properties.getProperty("LOG_PATH") + dateTime[0] + "/";

        File directory = new File(directoryPath);
        if (!directory.exists()) directory.mkdir();

        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        if (defaultPort)
            builder.usingPort(Integer.parseInt(_properties.getProperty("DEFAULT_PORT")));
        else
            builder.usingAnyFreePort();
        builder
                .withCapabilities(Driver.getCapabilities())
                .withLogFile(new File(directoryPath
                        + String.format("%s-", dateTime[1])
                        + _properties.getProperty("LOG_NAME")));
        _service = AppiumDriverLocalService.buildService(builder);
        _service.clearOutPutStreams();
        return _service;
    }
}
