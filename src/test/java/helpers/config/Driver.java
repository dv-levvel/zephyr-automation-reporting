package helpers.config;

import helpers.Utilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.Properties;

public class Driver {

    private static AndroidDriver<AndroidElement> _driver;
    private static DesiredCapabilities _caps;
    private static final Properties _properties = Utilities.extractProperties("src/test/resources/android.properties");

    public static AndroidDriver<AndroidElement> getInstance() {
        try {
            if (_driver == null) {
                System.out.println("Creating a driver instance...");
                _driver = new AndroidDriver<AndroidElement>(new URL(_properties.getProperty("URL")), getCapabilities());
            }
        } catch (Exception e) {
            System.out.println("Cause: " + e.getCause());
            System.out.println("Message: " + e.getMessage());
        }
        return _driver;
    }

    public static DesiredCapabilities getCapabilities() {
        if (_caps == null) {
            System.out.println("Setting up the device capabilities...");
            _caps = new DesiredCapabilities();
            _caps.setCapability(MobileCapabilityType.PLATFORM_NAME, _properties.getProperty("EMU_PLATFORM_NAME"));
            _caps.setCapability("VERSION", _properties.getProperty("EMU_VERSION"));
            _caps.setCapability(MobileCapabilityType.DEVICE_NAME, _properties.getProperty("EMU_DEVICE_NAME"));
            _caps.setCapability(MobileCapabilityType.UDID, _properties.getProperty("EMU_UDID"));
//        _caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, _properties.getProperty("APP_PACKAGE"));
//        _caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, _properties.getProperty("APP_ACTIVITY"));
            _caps.setCapability(MobileCapabilityType.BROWSER_NAME, _properties.getProperty("BROWSER_NAME"));
        }
        return _caps;
    }

    public static void closeDriver() {
        if (_driver != null) {
            _driver.quit();
            _driver = null;
        }
    }
}
